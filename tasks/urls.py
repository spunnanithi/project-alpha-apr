from django.urls import path
from tasks.views import (
    create_task,
    show_my_tasks,
    edit_task,
    individual_task,
    delete_task,
)


urlpatterns = [
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/", individual_task, name="individual_task"),
    path("create/", create_task, name="create_task"),
    path("<int:id>/edit/", edit_task, name="edit_task"),
    path("<int:id>/delete/", delete_task, name="delete_task"),
]

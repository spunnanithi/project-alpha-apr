# Generated by Django 4.1.5 on 2023-02-02 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0002_task_notes"),
    ]

    operations = [
        migrations.AddField(
            model_name="task",
            name="updated_date",
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name="task",
            name="due_date",
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name="task",
            name="start_date",
            field=models.DateTimeField(null=True),
        ),
    ]
